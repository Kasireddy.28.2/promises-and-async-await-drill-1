const fsPromises=require("fs").promises;
const path=require("path");

function callBacks(){
    fsPromises.readFile("../../../Downloads/lipsum_1.txt","utf-8")
    .then((data)=>{
        console.log("lipsum data readed Successfully");
        return data;
    }).then((data)=>{
        let upperCase=data.toUpperCase();
        let upperCaseFilePath="./upperCase.txt";
        fsPromises.writeFile(upperCaseFilePath,upperCase)
        console.log(`${upperCaseFilePath} written successfully`)
        return upperCaseFilePath

    }).then((upperCaseFilePath)=>{
        fsPromises.writeFile("./fileNames.txt",upperCaseFilePath+"\n")
        return upperCaseFilePath
    }).then((upperCaseFilePath)=>{
        return fsPromises.readFile(upperCaseFilePath,"utf-8")

    }).then((upperCaseData)=>{
        let lowerCaseData=upperCaseData.toLowerCase();
        let split=lowerCaseData.split(".");
        let splitFile="./split.txt"
        fsPromises.writeFile(splitFile,JSON.stringify(split))
        console.log(`${splitFile} created successfully`)
        return splitFile;
    }).then((splitFile)=>{
        fsPromises.appendFile("./fileNames.txt",splitFile+"\n")
        return splitFile;
    }).then((splitFile)=>{
        return fsPromises.readFile(splitFile,"utf-8")
    }).then((splitData)=>{
        let sortContent=JSON.parse(splitData).sort();
        let sortFile="./sort.txt";
        fsPromises.writeFile(sortFile,JSON.stringify(sortContent))
        console.log(`${sortFile} created successfully`);
        return sortFile
    }).then((sortFile)=>{
      fsPromises.appendFile("./fileNames.txt",sortFile)

    }).then(()=>{
        return fsPromises.readFile("./fileNames.txt","utf-8")
    }).then((filenamesData)=>{
        let fileNamesArray=filenamesData.split("\n");
        return fileNamesArray
    }).then((fileNamesArray)=>{
        for(let file of fileNamesArray){
            fsPromises.readFile(file,"utf-8")
            .then(()=>{
                console.log(`${file} readed successfully`);
            }).then(()=>{
                fsPromises.unlink(file)
                .then(()=>{
                    console.log(`${file} deleted successfully`);
                })
            })
        }
        fsPromises.unlink("./fileNames.txt");

    }).catch((error)=>{
        console.log(`error occured : ${error}`);
    })

               
}


module.exports=callBacks;