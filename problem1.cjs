const fsPromises=require("fs").promises;
const path=require("path");

function creatingAndDeletingFiles(){
    fsPromises.mkdir("./randomFiles",{recursive:true})
    .then(()=>{
        console.log("Directory Created Successfully");
    }).catch((error)=>{
        console.log(`error occured : ${error}`);
    });

    for(let index=1;index<4;index++){
        let fileName=`file${index}.txt.json`;
        let dir="./randomFiles";
        let filePath=path.join(dir,fileName);
        let file=`file${index}`
        let content=`file${index} created`;

        fsPromises.writeFile(filePath,JSON.stringify(content))
        .then(()=>{
            console.log(`${file} Created Successfully`);
        }).then(()=>{
            fsPromises.unlink(filePath)
            .then(()=>{
                console.log(`${file} Deleted Successfully`);
            }).catch((error)=>{
                console.log(`error occured :${error}`);
            })
            
        }).catch((error)=>{
            console.log(`error occured :${error}`);
        })
    }

    fsPromises.rm("./randomFiles",{recursive:true})
    .then(()=>{
        console.log("Directory Deleted Successfully");
    }).catch((error)=>{
        console.log(`error occured : ${error}`);
    })

    

};

module.exports=creatingAndDeletingFiles;
